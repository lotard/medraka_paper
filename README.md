# Medraka Paper

Scripts for recreating files and figures from "Cas9-induced large deletions 
and small indels are controlled in a convergent fashion" paper.

`medraka_figures.R` is the main script calling the preparatory scripts (`miseq_prep.R` and `fcm_prep.R`)
and creating the figures.

